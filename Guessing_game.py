import random

name=input("What is your name ?")
want_to_play = input("Hi, {}, would you like to play the guessing game? (Enter Yes/No) ".format(name))
print(" ")
print("The rules of the game : You have only three attempts and you must find out the number before your chances run out  ")
print(" ")

random_number=random.randint(0,10)
count=0
while count <=3:
    input_number = int(input(" Guess a number between 1 and 10 --->"))
    if input_number == random_number:
        print("Yes you have guessed it right !!")
        break
    else:
        print("Guess again ")

    count=count+1
    if count ==3 :
        print("Oops,you are out of chances !!")
        break

print("The number is : ", random_number)






